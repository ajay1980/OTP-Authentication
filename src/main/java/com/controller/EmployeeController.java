package com.controller;

import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ui.Model;

@Controller
public class EmployeeController {
	String numbers = "0123456789";
	int otpLength = 4;
	char[] otp = new char[otpLength];
	 
	@RequestMapping(value = "/OTP",method = RequestMethod.GET)
	public char[] otpGeneration() {
		System.out.println("Inside otpGeneration method");
		Random random = new Random();
		
		for (int i = 0; i < otpLength; i++)
        {
            otp[i] = numbers.charAt(random.nextInt(numbers.length()));
        }
        return otp;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String otpVarification(@PathVariable(value="id") int userOtp) {
		String VarificationResult = "OTP Varification failed";
		int generatedOtp = Integer.parseInt(String.valueOf(otp));
		if(userOtp == generatedOtp)
		{
			VarificationResult = "OTP successfully Varified !!";
		}
		
		return VarificationResult;
	}
	
	// inject via application.properties
		@Value("${welcome.message:test}")
		private String message = "Hello World";
		
	@RequestMapping("/")
	public String welcome(Model model) {
		model.addAttribute("message", this.message);
		model.addAttribute("OTP",otp);
		System.out.println("------------  "+message);
		return "Welcome";
	}
}
